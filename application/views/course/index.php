<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <head>
        <meta charset="utf-8">
        <title>Course</title>
    </head>
    <body>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg" align="center">
                    <h1>Course (UAH)</h1>
                    <hr><br>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <table class="table table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Currency</th>
                                <th scope="col">Buy</th>
                                <th scope="col">Sale</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($courses as $course): ?>
                                <tr>
                                    <th><?php echo $course->ccy; ?></th>
                                    <td><?php echo $course->buy; ?></td>
                                    <td><?php echo $course->sale; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>

            </div>
            <div class="row">
                <div class="col">
                    <a class="btn btn-info" href="<?php echo site_url('archive'); ?>">Archive</a>
                </div>
                <div class="col" align="right">
                    <?php echo standard_date('DATE_RFC822', time()) ?>
                </div>
            </div>

    </body>
</html>