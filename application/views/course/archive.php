<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <head>
        <meta charset="utf-8">
        <title>Archive</title>
    </head>
    <body>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg" align="center">
                    <h1>Archive</h1>
                    <hr><br>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <table class="table table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Currency</th>
                                <th scope="col">Buy</th>
                                <th scope="col">Sale</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; foreach ($courses as $course): ?>
                                <tr> 
                                    <td><?php echo $course->id; ?></td>
                                    <td><?php echo $course->currency; ?></td>
                                    <td><?php echo $course->buy; ?></td>
                                    <td><?php echo $course->sale; ?></td>
                                    <td><?php echo standard_date('DATE_RFC822', $course->date) ?></td>
                                </tr>
                                    <?php if ($i%2 == 1): ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                    <?php endif; ?>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>

                </div>

            </div>
    </body>
</html>