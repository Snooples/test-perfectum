<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course_controller extends CI_Controller
{

    const DEFAULT_CACHE_EXPERIED_TIME = 10; # 10 minutes

    public function __construct()
    {
        parent::__construct();
        $this->load->model('course');
    }

    public function index()
    {
        $courses = $this->course->getCurrentCourses();
        
        $this->output->cache(self::DEFAULT_CACHE_EXPERIED_TIME);

        if ($this->course->saveCourse()) {
            $this->load->view('course/index', [
                'courses' => $courses,
            ]);
        } else {
            show_error('Error!');
        }
    }

    public function archive()
    {
        $data = $this->course->getCourses();
        
        if (!$data) {
            show_error('Error!');
        }
        
        $this->load->view('course/archive', [
                'courses' => $data,
            ]);
    }

}
