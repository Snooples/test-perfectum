<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Model
{

    const API_LINK = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

    public $permittedCourses = ['USD', 'EUR'];

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getCurrentCourses()
    {
        $data = file_get_contents(self::API_LINK);
        $data = json_decode($data);

        $permitterCourses = $this->permittedCourses;

        foreach ($data as $course) {
            if (in_array($course->ccy, $permitterCourses)) {
                $courses[] = $course;
            }
        }

        return $courses;
    }

    public function saveCourse()
    {
        $data = $this->getCurrentCourses();

        $i = 0;
        $courses = [];

        foreach ($data as $item) {
            $i++;
            $courses[$i]['currency'] = $item->ccy;
            $courses[$i]['buy'] = $item->buy;
            $courses[$i]['sale'] = $item->sale;
            $courses[$i]['date'] = now();
        }


        foreach ($courses as $course) {
            $this->db->set($course);
            if (!$this->db->insert('course', $course)) {
                return false;
            }
        }
        return true;
    }

    public function getCourses()
    {
        $query = $this->db
                ->order_by('id', 'DESC')
                ->get('course')
                ->result();

        return $query;
    }

}
